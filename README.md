# Synergy

Use the keyboard, mouse, or trackpad of one computer to control nearby computers, and work seamlessly between them.

- Download: [Get Synergy](https://symless.com/synergy)
- Source code: [Deskflow](https://deskflow.org) (Synergy community upstream)
